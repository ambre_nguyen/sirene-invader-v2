# Sirene Invader V2 #

## Welcome to Sirene Invader ! ##
👾🧜‍♀️👾🧜‍♀️👾🧜‍♀️👾🧜‍♀️👾🧜‍♀️👾🧜‍♀️👾🧜‍♀️👾🧜‍♀️👾🧜‍♀️

This project aims to retrieve data from *https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/* and sorting it into a MongoDB database.
The following instructions will guide you through all available features. 

---

## Available Commands ##

- **Start**: `npm run start` or `pm2 start process.json`
  - Description: Starts the Sirene Invader process using PM2, which manages the process and ensures it stays running.
  
- **Stop**: `npm run stop` or `pm2 stop process.json`
  - Description: Stops the Sirene Invader process managed by PM2.

- **Flush Logs**: `npm run flush` or `pm2 flush`
  - Description: Flushes logs for all PM2-managed processes. Useful for clearing old logs and freeing up disk space.

- **Delete All Processes**: `npm run delete` or `pm2 delete all`
  - Description: Deletes all processes managed by PM2. Use with caution, as this will stop and remove all running processes.

- **View Logs**: `npm run logs` or `pm2 log`
  - Description: Displays logs for all PM2-managed processes. Useful for monitoring process output and debugging.

- **Decompose Data**: `npm run decompose` or `node decompose.js`
  - Description: Runs the `decompose.js` script, which decomposes data retrieved from the Sirene database and sorts it into a MongoDB database. This script is likely part of the Sirene Invader functionality.

*Have fun invading !* 