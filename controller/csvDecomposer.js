const fs = require('fs-extra');
const readline = require('readline');

const inputFile = 'StockEtablissement_utf8.csv'; // Input CSV file to decompose
const outputDir = './output/'; // Output directory for smaller files
const chunkSize = 30000; // Size of each chunk

// Ensure output directory exists
fs.ensureDirSync(outputDir);

const startTime = new Date();

// Function to split CSV into smaller chunks
function splitCSVIntoChunks() {
    const inputStream = fs.createReadStream(inputFile);
    const rl = readline.createInterface({
        input: inputStream,
        crlfDelay: Infinity
    });

    let chunkIndex = 0;
    let lines = [];
    let lineNumber = 0;

    rl.on('line', (line) => {
        lineNumber++;
        lines.push(line);

        if (lineNumber % chunkSize === 0) {
            writeChunkToFile(lines, ++chunkIndex);
            lines = []; // Reset lines array
        }
    });

    rl.on('close', () => {
        // Write remaining lines to file
        if (lines.length > 0) {
            writeChunkToFile(lines, ++chunkIndex);
        }
        console.log('Decomposition complete!');
        printElapsedTime();
    });

    rl.on('error', (err) => {
        console.error('Error reading input file:', err);
    });
}

// Function to write chunk of lines to file
function writeChunkToFile(lines, index) {
    const outputFileName = `${outputDir}chunk${index}.csv`;
    console.log(`Creating ${outputFileName}...`);
    const outputStream = fs.createWriteStream(outputFileName);
    outputStream.write(lines.join('\n') + '\n');
    outputStream.end();
}

// Function to print elapsed time
function printElapsedTime() {
    const endTime = new Date();
    const elapsedTime = endTime - startTime;
    const minutes = Math.floor(elapsedTime / 60000);
    const seconds = Math.floor((elapsedTime % 60000) / 1000);
    console.log(`Total processing time: ${minutes} minutes ${seconds} seconds`);
}

// Start the process
splitCSVIntoChunks();
