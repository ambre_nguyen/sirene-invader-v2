const fs = require('fs');
const { MongoClient } = require('mongodb');
const readline = require('readline');

const outputDir = './output/';

// MongoDB connection URL
const mongoURL = 'mongodb://localhost:27017';

const requiredFields = [
    'siren', 'nic', 'siret', 'dateCreationEtablissement',
    'dateDernierTraitementEtablissement', 'typeVoieEtablissement',
    'libelleVoieEtablissement', 'codePostalEtablissement',
    'libelleCommuneEtablissement', 'codeCommuneEtablissement',
    'dateDebut', 'etatAdministratifEtablissement'
];

// Function to connect to MongoDB
async function connectToMongo() {
    try {
        const client = new MongoClient(mongoURL); // Removed deprecated options
        await client.connect();
        console.log('Connected to MongoDB successfully');
        return client.db('sirene').collection('establishments');
    } catch (error) {
        console.error('Error connecting to MongoDB:', error);
        process.exit(1);
    }
}

// Function to check if all required fields are present and not empty
function isValidData(data) {
    // Check if any required field is empty
    return requiredFields.every(field => data[field] !== "");
}

// Function to remove unwanted fields from the object
function removeUnwantedFields(data) {
    const cleanedData = {};
    // Filter out unwanted fields
    Object.keys(data).forEach(key => {
        if (requiredFields.includes(key)) {
            cleanedData[key] = data[key];
        }
    });

    return cleanedData;
}


// Function to process a chunk of CSV data
async function processChunk(fileName, collection) {
    console.log(`Processing ${fileName}...`);
    const fileStream = fs.createReadStream(fileName);
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    let isFirstLine = true;
    let fieldNames = [];

    for await (const line of rl) {
        const fields = line.split(',');

        // Use the first line as field names
        if (isFirstLine) {
            fieldNames = fields.map(field => field.trim());
            isFirstLine = false;
            continue; // Skip to the next line
        }

        const cleanedData = {};

        // Map fields to cleanedData object using fieldNames as keys
        fields.forEach((field, index) => {
            cleanedData[fieldNames[index]] = field.trim();
        });

        // Remove unwanted fields
        const sanitizedData = removeUnwantedFields(cleanedData);

        // Check if data is valid before inserting into MongoDB
        if (isValidData(sanitizedData)) {
            try {
                await collection.insertOne(sanitizedData);
                console.log(`Data inserted into MongoDB`);
            } catch (error) {
                console.error(`Error inserting data into MongoDB:`, error);
            }
        } else {
            console.log(`Skipping line due to missing or empty required fields:`, line);
        }
    }

    console.log(`Finished processing ${fileName}`);
}

// Function to print elapsed time
function printElapsedTime() {
    const endTime = new Date();
    const elapsedTime = endTime - startTime;
    const minutes = Math.floor(elapsedTime / 60000);
    const seconds = Math.floor((elapsedTime % 60000) / 1000);
    console.log(`Total processing time: ${minutes} minutes ${seconds} seconds`);
}



// Main function
async function main() {
    const startTime = new Date();
    const collection = await connectToMongo();
    const files = fs.readdirSync(outputDir);
    for (const file of files) {
        await processChunk(outputDir + file, collection);
    }
    console.log('All files processed successfully');
    printElapsedTime();
    process.exit(0);
    let end = performance.now();
    console.log(end - start);
}

main();